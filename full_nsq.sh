#!/bin/bash

FILE="bench_nsq_$(date +%s)"
echo "" > ~/$FILE
for NODES in 5 10 20 100
do
    for MESS in 50000 100000 200000
    do
        for MSS in 0 2 5
        do
            i=4
            while [ "$i" != 0 ]
            do
                wait $(./nsq_test_csv.sh -w $NODES -c $MESS -t $MSS &>>~/$FILE)
                let "i -= 1"
            done
        done
    done
done