#!/bin/bash

START_TIME=$(date +%s)
sleep 1
c=0
count=0
h=0
while true;do
    STATS=$(curl -s 'http://localhost:4151/stats')
    DEPTH=$(echo  "$STATS"|grep channel|awk '{print $4}')
    FIN_SUM=$(echo  "$STATS"|grep -e 'localhost:'|awk '{print $11}'|awk '{s += $1} END {print s}')
    NOW_TIME=$(date +%s)
    SUM_TIME=$[$NOW_TIME-$START_TIME]

    let "c = $SUM_TIME % 10"
    let "count += 1"
    let "h = $count % 2"
    if [ "$c" == 0 ] &&  [ "$h" == 0 ] ; then
        echo "Now DEPTH: $DEPTH | TOTAL_TIME: $SUM_TIME"
    fi

    if [ "$DEPTH" == "0" ] ; then
        echo -e "End of wait"
        exit 0
    fi
    sleep 0.2
done