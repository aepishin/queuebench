package producer

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"queuebench"
	"syscall"
	"time"
	"github.com/bitly/go-nsq"
)

// Application exterminator
type Application struct {
	fBreak *queuebench.FlagBreak

	config *nsq.Config
	w      *nsq.Producer
	CountMessages int
}

// NewApplication creates and initializes new instance of Application
func NewApplication() *Application {
	return &Application{}
}

// Run the application. It should be initialized before this call
func (this *Application) Run() {
	this.fBreak = &queuebench.FlagBreak{}
	i := 0
	for {
		i++

		err := this.w.Publish("queue_test", []byte(fmt.Sprintf("message %v", i)))
		if err != nil {
			log.Fatalf("Could not connect")
		}
//		log.Printf(fmt.Sprintf("message %v", i))
		time.Sleep(0)
		if this.fBreak.Get() {
			break
		}

		if i >= this.CountMessages {
			break
		}
	}
}

// Init initializes sara application and all underlying services like cache, DB, etc
func (this *Application) Init(configDir, env string) error {

	// register shutdown function
	this.registerShutdownFunction()

	this.InitNSQ()

	return nil
}

// Done close connection
func (this *Application) Done() {
	this.w.Stop()
	log.Print("Interrupt application")
	this.fBreak.Set(true)
}

// registerShutdownFunction handle for kill application event (Ctrl+C for example)
func (this *Application) registerShutdownFunction() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-c
		this.Done()
	}()
}
