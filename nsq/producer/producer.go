package producer

import "github.com/bitly/go-nsq"

func (this *Application) InitNSQ() {
	this.config = nsq.NewConfig()
	this.w, _ = nsq.NewProducer("127.0.0.1:4150", this.config)
}
