package consumer

import (
	"log"
	"os"
	"os/signal"
	"queuebench"
	"syscall"
	"time"
	"github.com/bitly/go-nsq"
)

// Application exterminator
type Application struct {
	fBreak  *queuebench.FlagBreak
	counter int
	q       *nsq.Consumer
	TimeOut int
}

// NewApplication creates and initializes new instance of Application
func NewApplication() *Application {
	return &Application{}
}

// Run the application. It should be initialized before this call
func (this *Application) Run() {

	this.fBreak = &queuebench.FlagBreak{}
	i := 0
	for {
		i++

		time.Sleep(0)
		if this.fBreak.Get() {
			break
		}
	}

}

// Init initializes sara application and all underlying services like cache, DB, etc
func (this *Application) Init(configDir, env string) error {

	// register shutdown function
	this.registerShutdownFunction()

	this.InitConsumer()

	return nil
}

func (this *Application) InitConsumer() {
	config := nsq.NewConfig()
	channelName := "channel"
	this.q, _ = nsq.NewConsumer("queue_test", channelName, config)
	this.q.AddHandler(nsq.HandlerFunc(func(message *nsq.Message) error {
//		log.Printf("Got a message: %v", message)
		time.Sleep(time.Duration(this.TimeOut) * time.Millisecond)
		return nil
	}))
	err := this.q.ConnectToNSQD("127.0.0.1:4150")
	if err != nil {
		log.Fatalf("Could not connect, %v", err)
	}
}

// Done close connection
func (this *Application) Done() {
	this.q.DisconnectFromNSQD("127.0.0.1:4150")
	log.Print("Interrupt application")
	this.fBreak.Set(true)
}

// registerShutdownFunction handle for kill application event (Ctrl+C for example)
func (this *Application) registerShutdownFunction() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-c
		this.Done()
	}()
}
