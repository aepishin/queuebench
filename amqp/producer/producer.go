package producer

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

func (this *Application) InitAmqp() (err error) {

	var (
		uri          = "amqp://guest:guest@localhost:5672/"
		exchangeType = "direct"
	)

	this.exchangeName = "test-exchange"
	this.routingKey = "test-key"

	log.Printf("dialing %q", uri)
	this.conn, err = amqp.Dial(uri)
	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}

	log.Printf("got Connection, getting Channel")
	this.channel, err = this.conn.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	log.Printf("got Channel, declaring %q Exchange (%q)", exchangeType, this.exchangeName)
	if err := this.channel.ExchangeDeclare(
		this.exchangeName, // name
		exchangeType,      // type
		true,              // durable
		false,             // auto-deleted
		false,             // internal
		false,             // noWait
		nil,               // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	return
}

func (this *Application) publish(body string) (err error) {

	//	log.Printf("declared Exchange, publishing %dB body (%q)", len(body), body)
	if err = this.channel.Publish(
		this.exchangeName, // publish to an exchange
		this.routingKey,   // routing to 0 or more queues
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            []byte(body),
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}
