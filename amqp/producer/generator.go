package producer

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"queuebench"
	"syscall"
	"time"
	"github.com/streadway/amqp"
)

// Application exterminator
type Application struct {
	fBreak *queuebench.FlagBreak

	conn         *amqp.Connection
	channel      *amqp.Channel
	exchangeName string
	routingKey   string
	CountMessages int
}

// NewApplication creates and initializes new instance of Application
func NewApplication() *Application {
	return &Application{}
}

// Run the application. It should be initialized before this call
func (this *Application) Run() {
	this.fBreak = &queuebench.FlagBreak{}
	i := 0
	for {
		i++
		this.publish(fmt.Sprintf("message %v", i))

		time.Sleep(0)
		if this.fBreak.Get() {
			break
		}
		if i >= this.CountMessages {
			break
		}
	}
}

// Init initializes sara application and all underlying services like cache, DB, etc
func (this *Application) Init(configDir, env string) error {

	// register shutdown function
	this.registerShutdownFunction()

	this.InitAmqp()

	return nil
}

// Done close connection
func (this *Application) Done() {
	this.conn.Close()
	log.Print("Interrupt application")
	this.fBreak.Set(true)
}

// registerShutdownFunction handle for kill application event (Ctrl+C for example)
func (this *Application) registerShutdownFunction() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-c
		this.Done()
	}()
}
