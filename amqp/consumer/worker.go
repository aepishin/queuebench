package consumer

import (
	"log"
	"os"
	"os/signal"
	"queuebench"
	"syscall"
	"time"
)

// Application exterminator
type Application struct {
	fBreak  *queuebench.FlagBreak

	consumer *Consumer
	TimeOut int
}

// NewApplication creates and initializes new instance of Application
func NewApplication() *Application {
	return &Application{}
}

// Run the application. It should be initialized before this call
func (this *Application) Run() {
	this.fBreak = &queuebench.FlagBreak{}
	var err error
	this.consumer, err = this.NewConsumer(*uri, *exchange, *exchangeType, *queue, *bindingKey, *consumerTag)
	if err != nil {
		log.Fatalf("%s", err)
	}

	if *lifetime > 0 {
		log.Printf("running for %s", *lifetime)
		time.Sleep(*lifetime)
	} else {
		log.Printf("running forever")
		for {
			time.Sleep(0)
			if this.fBreak.Get() {
				break
			}
		}
	}
}

// Init initializes sara application and all underlying services like cache, DB, etc
func (this *Application) Init(configDir, env string) error {

	// register shutdown function
	this.registerShutdownFunction()

	return nil
}

// Done close connection
func (this *Application) Done() {
	if err := this.consumer.Shutdown(); err != nil {
		log.Fatalf("error during shutdown: %s", err)
	}
	log.Print("Interrupt application")
	this.fBreak.Set(true)
}

// registerShutdownFunction handle for kill application event (Ctrl+C for example)
func (this *Application) registerShutdownFunction() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-c
		this.Done()
	}()
}
