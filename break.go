package queuebench

import "sync"

type FlagBreak struct {
	flag bool
	m    sync.Mutex
}

func (f *FlagBreak) Get() bool {
	f.m.Lock()
	result := f.flag
	f.m.Unlock()

	return result
}

func (f *FlagBreak) Set(flag bool) {
	f.m.Lock()
	f.flag = flag
	f.m.Unlock()
}
