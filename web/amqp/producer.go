package main

import (
	"flag"
	"log"
	"queuebench/amqp/producer"
)

func main() {

	// Load config
	var env, configDir string
	var countMessages int
	flag.StringVar(&env, "env", "", "Environment. Environment-specific confic file will overwrite core config file 'app.ini'")
	flag.StringVar(&configDir, "config_dir", "../etc", "Directory to config files")
	flag.IntVar(&countMessages, "c", 50000, "Count messages")
	flag.Parse()

	app := producer.NewApplication()
	app.CountMessages = countMessages
	if err := app.Init(configDir, env); err != nil {
		log.Fatalf("Error run application: %v", err)
	}
	app.Run()

}
