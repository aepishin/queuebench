package main

import (
	"flag"
	"log"
	"queuebench/nsq/consumer"
)

func main() {

	// Load config
	var env, configDir string
	var timeOut int
	flag.StringVar(&env, "env", "", "Environment. Environment-specific confic file will overwrite core config file 'app.ini'")
	flag.StringVar(&configDir, "config_dir", "../etc", "Directory to config files")
	flag.IntVar(&timeOut, "t", 0, "Count messages")
	flag.Parse()

	app := consumer.NewApplication()
	app.TimeOut = timeOut
	if err := app.Init(configDir, env); err != nil {
		log.Fatalf("Error run application: %v", err)
	}
	app.Run()

}
