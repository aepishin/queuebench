#!/bin/bash

START_TIME=$(date +%s)
count=0
DEPTH=0
while true;do
    STATS=$(curl -s 'http://localhost:4151/stats')
    DEPTH=$(echo  "$STATS"|grep channel|awk '{print $4}')
    let "count += 1"
    if [ "$DEPTH" -gt 0 ] || [ "$count" -gt 100 ] ; then
        exit 0
    fi
    sleep 0.1
done
