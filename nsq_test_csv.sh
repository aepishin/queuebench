#!/bin/bash

workercount=0
messagescount=0
timeout=1000000000000
while getopts ":w:c:t:" opt; do
    case $opt in
        w)
          workercount=$OPTARG
          ;;
        c)
          messagescount=$OPTARG
          ;;
        t)
          timeout=$OPTARG
          ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          exit 1
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          exit 1
          ;;
    esac
done

if [ "$workercount" == 0 ] ; then
    echo -e "Invalid option: -w" >&2
    exit 1
fi
if [ "$messagescount" == 0 ] ; then
    echo -e "Invalid option: -c" >&2
    exit 1
fi
if [ "$timeout" == 1000000000000 ] ; then
    echo -e "Invalid option: -t" >&2
    exit 1
fi

killall nsq_consumer &>/dev/null
curl 'http://localhost:4151/delete_topic?topic=queue_test' &>/dev/null

#microseconds
x=`date +"%Y-%m-%d %H:%M:%S"` ; y=`echo "\`date +%N\` / 1000" | bc` ;
TIME1="$x.$y"
WORKERS=$workercount

while [ "$workercount" -gt 0 ]
do
    let "workercount -= 1"
    ./nsq_consumer -env dev -config_dir=/home/aepishin/go/src/queuebench/etc -t $timeout &>/dev/null &
done
sleep 2
PUBLISH_TIME=$((time (./nsq_producer -env dev -config_dir=/home/aepishin/go/src/queuebench/etc -c $messagescount &>/dev/null) 2>&1) | grep real | awk '{print $2}')

(wait $(./nsq_wait_timer.sh &>/dev/null ) 2>&1)

HANDLE_TIME=$((time (./nsq_test_timer.sh &>/dev/null ) 2>&1) | grep real | awk '{print $2}')

x=`date +"%Y-%m-%d %H:%M:%S"` ; y=`echo "\`date +%N\` / 1000" | bc` ;
TIME2="$x.$y"

killall nsq_consumer  &>/dev/null

PARTS=$(echo $PUBLISH_TIME | sed 's/s//g' | sed 's/m/ /g'| sed 's/\./ /g')
MIN=$(echo $PARTS |  awk '{print $1}')
SEC=$(echo $PARTS |  awk '{print $2}')
MICROSEC=$(echo $PARTS |  awk '{print $3}')
PUBLISH_TIME=$[$MIN*60+$SEC].$MICROSEC

PARTS=$(echo $HANDLE_TIME | sed 's/s//g' | sed 's/m/ /g'| sed 's/\./ /g')
MIN=$(echo $PARTS |  awk '{print $1}')
SEC=$(echo $PARTS |  awk '{print $2}')
MICROSEC=$(echo $PARTS |  awk '{print $3}')
HANDLE_TIME=$[$MIN*60+$SEC].$MICROSEC

echo -e "$WORKERS;$timeout;$messagescount;$PUBLISH_TIME;$HANDLE_TIME;$TIME1;$TIME2;"

exit 0