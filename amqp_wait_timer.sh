#!/bin/bash

START_TIME=$(date +%s)
count=0
DEPTH=0
while true;do
    STATS=$(curl -s -u guest:guest 'http://localhost:8080/api/overview')
    DEPTH=$(echo  "$STATS"| json_pp | grep \"messages\" | sed 's/://g'| awk '{print $2}' | tr -d ',')
    let "count += 1"
    if [ "$DEPTH" -gt 0 ] || [ "$count" -gt 100 ] ; then
        exit 0
    fi
    sleep 0.1
done
