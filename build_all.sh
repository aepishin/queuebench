#!/usr/bin/env bash
#nsq
go build -o nsq_consumer web/nsq/consumer.go
go build -o nsq_producer web/nsq/producer.go

#amqp
go build -o amqp_consumer web/amqp/consumer.go
go build -o amqp_producer web/amqp/producer.go